import { Composer } from 'grammy';
import { CustomContext } from 'src/bot';
import { banhammerCommand } from './banhammer';
import { perdonaCommand } from './perdona';
import { punisciCommand } from './punisci';

export const commands = new Composer<CustomContext>();

commands.command('banhammer', banhammerCommand);
commands.command('perdona', perdonaCommand);
commands.command('punisci', punisciCommand);
