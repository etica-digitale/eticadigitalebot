import { CustomContext } from 'src/bot';
import { getUserTag } from '../utils';
import { handleAdminCommand } from './admin-command-handler';

export const banhammerCommand = async (ctx: CustomContext) => {
  handleAdminCommand(ctx, (userToBan, reason) => {
    ctx.banChatMember(userToBan.id);
    if (!reason) {
      ctx.reply(`L'utente ${getUserTag(userToBan)} è stato rimosso\\.`, {
        parse_mode: 'MarkdownV2',
      });
    } else {
      ctx.reply(
        `L'utente ${getUserTag(
          userToBan,
        )} è stato rimosso\\.\nMotivo: ${reason}`,
        { parse_mode: 'MarkdownV2' },
      );
    }
  });
};
