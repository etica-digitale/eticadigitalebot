import { getRepository } from 'typeorm';
import { CustomContext } from 'src/bot';
import { User as UserEntity } from '../user';
import { User as GrammyUser } from '@grammyjs/types';

declare type AdminCommandHandler = (
  userToBan: GrammyUser,
  reason?: string,
) => void;

export async function handleAdminCommand(
  ctx: CustomContext,
  commandHandler: AdminCommandHandler,
) {
  if (ctx.chat.type === 'private' || ctx.chat.type === 'channel') {
    return;
  }
  if (!(await ctx.isAdmin(ctx.from.id))) {
    return;
  }
  let args = ctx.message.text.split(/ (.+)/);
  args.shift();
  args.pop();

  if (ctx.message.reply_to_message) {
    const userToBan = ctx.message.reply_to_message.from;
    if (await ctx.isAdmin(userToBan.id)) {
      return;
    }
    if (args.length < 1) {
      commandHandler(userToBan);
      return;
    }
    commandHandler(userToBan, args[0]);
    return;
  } else if (args.length < 1) {
    ctx.reply("Devi specificare l'id o l'username dell'utente\\.", {
      parse_mode: 'MarkdownV2',
    });
    return;
  } else {
    args = args[0].split(/ (.+)/);
    const userToBanArg = args.shift();
    if (userToBanArg.startsWith('@')) {
      const userRepository = getRepository(UserEntity);
      const userFound = await userRepository.findOne({
        username: userToBanArg,
      });
      if (!userFound) {
        ctx.reply('Utente non trovato\\.', { parse_mode: 'MarkdownV2' });
        return;
      }
      if (await ctx.isAdmin(userFound.id)) {
        return;
      }
      try {
        const userToBan = (await ctx.getChatMember(userFound.id)).user;
        if (args.length < 1) {
          commandHandler(userToBan);
        } else {
          commandHandler(userToBan, args[0]);
        }
      } catch (_) {}
    } else {
      if (!Number(userToBanArg)) {
        return;
      }
      if (await ctx.isAdmin(parseInt(userToBanArg, 10))) {
        return;
      }
      try {
        const userToBan = (await ctx.getChatMember(parseInt(userToBanArg, 10)))
          .user;
        if (args.length < 1) {
          commandHandler(userToBan);
        } else {
          commandHandler(userToBan, args[0]);
        }
      } catch (_) {
        ctx.reply('Utente non trovato\\.', { parse_mode: 'MarkdownV2' });
      }
    }
  }
}
