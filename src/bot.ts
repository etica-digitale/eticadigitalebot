import { config } from './config';
import {
    Bot,
    Context,
    GrammyError,
    HttpError,
    session,
    SessionFlavor,
} from 'grammy';
import { getRepository } from 'typeorm';
import { antiflood } from './antiflood';
import { User } from './user';
import 'reflect-metadata';
import { commands } from './commands/commands';
import { UtilsContext, utilsMiddleware } from './utils';
import { linkReplacer } from './link-replacer/link-replacer';

interface SessionData {
    floodUsers: { [id: number]: { messages: number; lastMessage: number } };
    limitedUsers: {
        [id: number]: { emoji: string; lastMessage: number; errors: number };
    };
}

export type CustomContext = Context & SessionFlavor<SessionData> & UtilsContext;

export const bot = new Bot<CustomContext>(config.token);

bot.use(
    session({
        initial(): SessionData {
            return { floodUsers: {}, limitedUsers: {} };
        },
    }),
);
bot.use(utilsMiddleware);

bot.on('msg', async (ctx, next) => {
    const userRepository = getRepository(User);
    const user = userRepository.create({
        id: ctx.from.id,
        username: ctx.from.username ? `@${ctx.from.username}` : null,
    });
    await userRepository.save(user);
    await next();
});

bot.use(antiflood);
bot.use(linkReplacer);
bot.use(commands);

bot.command("ping", async (ctx: CustomContext) => {
    ctx.reply("pong")
})

bot.catch((err) => {
    const ctx = err.ctx;
    console.error(`Error while handling update ${ctx.update.update_id}:`);
    const e = err.error;
    if (e instanceof GrammyError) {
        console.error('Error in request:', e.description);
    } else if (e instanceof HttpError) {
        console.error('Could not contact Telegram:', e);
    } else {
        console.error('Unknown error:', e);
    }
});
