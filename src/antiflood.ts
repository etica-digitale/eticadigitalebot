import { Composer } from 'grammy';
import { CustomContext } from './bot';
import { getUnixTime, subSeconds } from 'date-fns';
import { captcha, generateCaptcha } from './captcha';
import { config } from './config';
import { getUserTag } from './utils';

export const antiflood = new Composer<CustomContext>();
antiflood.use(captcha);

antiflood.on('message', async (ctx, next) => {
  const senderId = ctx.from.id;
  if (await ctx.isAdmin(senderId)) {
    next();
    return;
  }

  const floodUsers = ctx.session.floodUsers;
  const limitedUsers = ctx.session.limitedUsers;
  const floodingUser = floodUsers[senderId];
  if (!floodingUser) {
    floodUsers[senderId] = {
      messages: 1,
      lastMessage: getUnixTime(Date.now()),
    };
  } else {
    if (
      floodingUser.lastMessage <
      getUnixTime(subSeconds(Date.now(), config.antiflood.secondsRange))
    ) {
      floodUsers[senderId] = {
        messages: 1,
        lastMessage: getUnixTime(Date.now()),
      };
      next();
      return;
    }

    floodingUser.messages += 1;
    floodingUser.lastMessage = getUnixTime(Date.now());
    floodUsers[senderId] = floodingUser;

    if (floodUsers[senderId].messages >= config.antiflood.maxMessages) {
      // prevent from asking captcha and restricting the user if it was just done
      // I check that it is newer than the last 30 seconds because it is unlikely that an admin
      // will manually unrestrict the user that has flooded in less then 30 seconds
      if (
        senderId in limitedUsers &&
        limitedUsers[senderId].lastMessage >
          getUnixTime(subSeconds(Date.now(), 30))
      ) {
        return;
      }

      const emojis = Object.keys(config.captchaEmojis);
      const randomEmoji =
        config.captchaEmojis[emojis[(emojis.length * Math.random()) << 0]];
      limitedUsers[senderId] = {
        emoji: randomEmoji.code,
        lastMessage: getUnixTime(Date.now()),
        errors: 0,
      };
      ctx.restrictChatMember(senderId, {
        can_send_polls: false,
        can_change_info: false,
        can_invite_users: false,
        can_pin_messages: false,
        can_send_messages: false,
        can_send_media_messages: false,
        can_send_other_messages: false,
        can_add_web_page_previews: false,
      });
      ctx.reply(
        `${getUserTag(ctx.from)} clicca *${
          randomEmoji.description
        }* per risolvere il captcha\n*Errori*: ${
          limitedUsers[senderId].errors
        }`,
        {
          reply_markup: generateCaptcha(senderId),
          parse_mode: 'MarkdownV2',
        },
      );
      return;
    }
  }
  next();
  return;
});
